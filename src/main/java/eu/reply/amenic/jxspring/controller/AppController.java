package eu.reply.amenic.jxspring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from a Spring Application created by Jenkins-X!";
	}
	
}
